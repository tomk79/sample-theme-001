Pickles 2 - THEME SAMPLE
=========

[Pickles 2](http://pickles2.pxt.jp/) のテーマパッケージのサンプルです。

## 導入方法 - Setup

### 1. [Pickles 2](http://pickles2.pxt.jp/) をセットアップ

### 2. composer.json に、パッケージ情報を追加
```
{
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/tomk79/sample-theme-001.git"
        }
    ],
    "require": {
        "tomk79/px2-pickles2-sample-theme-001": "dev-master"
    }
}
```

### 3. composer update

更新したパッケージ情報を反映します。

```
$ composer update
```

### 4. config.php を更新

テーマを置き換えます。

```
<?php
return call_user_func( function(){

	/* (中略) */

	$conf->funcs->processor->html = [

		/* (中略) */

		// テーマ
		// 'theme'=>'picklesFramework2\theme\theme::exec' , // ←削除
		'theme'=>'tomk79\pickles2\sample\themes\theme::exec' , // ←追加

		/* (中略) */

	];

	/* (中略) */

	return $conf;
} );
```


## ライセンス - License

MIT License


## 作者 - Author

- (C)Tomoya Koyanagi <tomk79@gmail.com>
- website: <http://www.pxt.jp/>
- Twitter: @tomk79 <http://twitter.com/tomk79/>


